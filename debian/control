Source: augur
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               flake8 <!nocheck>,
               mafft,
               mypy <!nocheck>,
               python3-all,
               python3-setuptools,
               python3-pytest <!nocheck>,
               python3-pytest-mock <!nocheck>,
               python3-biopython <!nocheck>,
               python3-jsonschema <!nocheck>,
               python3-networkx <!nocheck>,
               python3-packaging <!nocheck>,
               python3-pandas <!nocheck>,
               python3-pyfastx <!nocheck>,
               python3-treetime (>= 0.9.4) <!nocheck>,
               python3-typeshed <!nocheck>,
               python3-freezegun <!nocheck>,
               python3-isodate <!nocheck>,
               python3-zstandard <!nocheck>,
               vcftools <!nocheck>,
               python3-xopen (>= 1.7.0-1~),
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/augur
Vcs-Git: https://salsa.debian.org/med-team/augur.git
Homepage: https://github.com/nextstrain/augur
Rules-Requires-Root: no

Package: augur
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-biopython,
         python3-boto3,
         python3-cvxopt,
         python3-dendropy,
         python3-jsonschema,
         python3-matplotlib,
         python3-packaging,
         python3-pandas,
         python3-pyfastx,
         python3-schedule,
         python3-seaborn,
         python3-treetime (>= 0.9.4),
         python3-zstandard,
         seqmagick,
         python3-ipdb,
         python3-bcbio-gff,
         mafft,
         raxml,
         fasttree,
         vcftools,
         python3-xopen (>= 1.7.0-1~)
Recommends: iqtree
Description: pipeline components for real-time virus analysis
 The nextstrain project is an attempt to make flexible informatic
 pipelines and visualization tools to track ongoing pathogen evolution as
 sequence data emerges. The nextstrain project derives from nextflu,
 which was specific to influenza evolution.
 .
 nextstrain is comprised of three components:
 .
   * fauna: database and IO scripts for sequence and serological data
   * augur: informatic pipelines to conduct inferences from raw data
   * auspice: web app to visualize resulting inferences
